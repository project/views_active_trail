# Views Active Trail

This module does a simple thing that I was missing from Drupal:
It adds an `active-trail` class to views list items if their target path is the
same with the current path.

## Example Scenario

You have a list of terms (view) displayed in a block and you want to highlight
the active term.

# Installation

 * Install [as you would normally install a contributed Drupal module]
(https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules).

# Maintainers

[Bill Seremetis (bserem)](https://drupal.org/u/bserem)
