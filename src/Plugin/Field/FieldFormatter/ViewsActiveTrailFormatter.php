<?php

/**
 * @file
 * Contains \Drupal\views_active_trail\Plugin\Field\FieldFormatter\ViewsActiveTrail.
 */



namespace Drupal\views_active_trail\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
/**
 * Plugin implementation of the 'views_active_trail' formatter.
 *
 * @FieldFormatter(
 *   id = "views_active_trail",
 *   label = @Translation("Link with Active Trail"),
 *   description = @Translation("Link with active class if current url"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class ViewsActiveTrailFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $current_path = \Drupal::service('path.current')->getPath();
    $current_url = \Drupal::service('path_alias.manager')->getAliasByPath($current_path, $langcode);

    foreach ($items as $delta => $item) {
      $entity = $item->getEntity();
      $url = $entity->toUrl()->toString();
      if ($url) {
        $elements[$delta] = [
          '#type' => 'link',
          '#title' => $item->value,
          '#url' => Url::fromUri('internal:' . $url)
        ];
        if ($url == $current_url) {
          $elements[$delta]['#attributes'] = [
            'class' => [
              'active-trail'
            ]
          ];
        }
      }
      else {
        $elements[$delta] = ['#markup' => $item->value];
      }
    }

    return $elements;
  }

}
